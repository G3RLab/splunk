* https://docs.splunk.com/Documentation/Splunk/8.0.1/Installation/DeployandrunSplunkEnterpriseinsideDockercontainers
https://hub.docker.com/r/splunk/splunk/
https://docs.docker.com/develop/develop-images/dockerfile_best-practices/
https://www.splunk.com/en_us/blog/tips-and-tricks/hands-on-lab-sandboxing-with-splunk-with-docker.html
https://www.nginx.com/blog/deploying-nginx-nginx-plus-docker/

* docker run -d -p 8000:8000 -e 'SPLUNK_START_ARGS=--accept-license' -e 'SPLUNK_PASSWORD=<password>'

* https://vsupalov.com/debug-docker-container/
sudo docker run -it --entrypoint /bin/bash splunk/splunk:latest -s